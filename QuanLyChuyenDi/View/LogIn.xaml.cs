﻿using QuanLyChuyenDi.Controller;
using QuanLyChuyenDi.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLyChuyenDi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click_1(object sender, RoutedEventArgs e)
        {
            string username = this.txtUserName.Text;
            string pass = this.txtPasswordBox.Password;
            if(AdminController.Instance().LogIn(username,pass))
            {
                this.Hide();
                if (WelcomeController.Instance().Check())
                {
                    new Welcome().Show();
                    return;
                }
                new Admin().Show();
            }
            else
            {
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
            } 
        }
    }
}
