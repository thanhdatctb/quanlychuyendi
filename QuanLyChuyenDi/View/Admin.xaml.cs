﻿using Microsoft.Win32;
using QuanLyChuyenDi.Controller;
using QuanLyChuyenDi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyChuyenDi.View
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        public Admin()
        {
            InitializeComponent();
            LoadData();
        }
        private void LoadMember()
        {
            this.dtgMember.ItemsSource = Controller.MemberController.Instance().GetAll();
            this.cbMemberOfTravel.ItemsSource = Controller.MemberController.Instance().GetAll();
            this.cbMemberOfTravel.DisplayMemberPath = "name";
            this.cbMemberOfTravel.SelectedValuePath = "id";

        }
        private void LoadTravel()
        {
            this.dtgTravel.ItemsSource = Controller.TravelController.Instance().GetAll();
            this.cbTravel.ItemsSource = Controller.TravelController.Instance().GetAll();
            this.cbTravelSpend.ItemsSource = Controller.TravelController.Instance().GetAll();
            this.cbTravel.DisplayMemberPath = "place";
            //this.cbTravel.DisplayMemberPath += "place";
            this.cbTravel.SelectedValuePath = "id";

            this.cbTravel_Copy.ItemsSource = Controller.TravelController.Instance().GetAll();
            this.cbTravel_Copy.DisplayMemberPath = "place";
            //this.cbTravel.DisplayMemberPath += "place";
            this.cbTravel_Copy.SelectedValuePath = "id";

            this.cbTravelSpend.DisplayMemberPath = "place";
            this.cbTravelSpend.SelectedValuePath = "id";
        }
        private void LoadData()
        {
            LoadMember();
            LoadTravel();
        }
        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Model.member member = new Model.member();
            member.name = this.TxtMemberName.Text;
            member.age =  Int32.Parse(this.TxtMemberAge.Text);
            member.status = true;
           
                if (Controller.MemberController.Instance().Add(member))
                {
                    MessageBox.Show("Thêm thành viên thành công");
                    //this.Hide();
                    //new Admin().Show();
                    LoadData();
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnEditMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Model.member member = new Model.member();
            member.id = Int32.Parse(this.TxtMemberId.Text);
            member.name = this.TxtMemberName.Text;
            member.age = Int32.Parse(this.TxtMemberAge.Text);
            member.status = true;
            
                if (Controller.MemberController.Instance().Edit(member))
                {
                    MessageBox.Show("Sửa thành viên thành công");
                    LoadData();
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnDeleteMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int id = Int32.Parse(this.TxtMemberId.Text);
            
                if (Controller.MemberController.Instance().Delete(id))
                {
                    MessageBox.Show("Xóa thành viên thành công");
                    LoadData();
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnAddTravel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                travel travel = new travel();
            travel.place = this.TxtTravelPlace.Text;
            travel.startTime = this.dpTravelStart.SelectedDate;
            travel.endTime = this.DpTravelEnd.SelectedDate;
           
                if (Controller.TravelController.Instance().Add(travel))
                {
                    MessageBox.Show("Thêm chuyến đi thành công");
                    LoadData();
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEditTravel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                travel travel = new travel();
            travel.id = Int32.Parse(this.TxtTravelId.Text);
            travel.place = this.TxtTravelPlace.Text;
            travel.startTime = this.dpTravelStart.SelectedDate;
            travel.endTime = this.DpTravelEnd.SelectedDate;
            
                if (Controller.TravelController.Instance().Edit(travel))
                {
                    MessageBox.Show("Sửa chuyến đi thành công");
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeleteTravel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                travel travel = new travel();
            int id = Int32.Parse(this.TxtTravelId.Text);
            travel.place = this.TxtTravelPlace.Text;
            travel.startTime = this.dpTravelStart.SelectedDate;
            travel.endTime = this.DpTravelEnd.SelectedDate;
            
                if (Controller.TravelController.Instance().Delete(id))
                {
                    MessageBox.Show("Xóa chuyến đi thành công");
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbTravel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int travelId = Int32.Parse(this.cbTravel.SelectedValue.ToString());
                var abc = Controller.MemberTravelController.Instance().GetMembersOfTravel(travelId); ;
                if(abc.Last() == null)
                {
                    var s = abc.Last();
                    abc.Remove(s);
                }    
               
                this.dtgMemberOfTravel.ItemsSource = abc;

            }catch(Exception ex)
            {
                
            }


        }

        private void btnAddMemberToTravel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //this.cbMemberOfTravel = new ComboBox();
                int travelId = Int32.Parse(this.cbTravel.SelectedValue.ToString());
                var memberValue = this.cbMemberOfTravel.SelectedValue.ToString();

                int memberId = Int32.Parse(memberValue);
            
                if(MemberTravelController.Instance().AddMemberToTravel(travelId, memberId))
                {
                    MessageBox.Show("Đã thêm thành viên vào chuyến đi này");
                    this.Hide();
                    new Admin().Show();

                    //this.dtgMemberOfTravel.ItemsSource = MemberTravelController.Instance().GetMembersOfTravel(travelId);
                }
                else
                {
                    MessageBox.Show("Thêm thành viên vào chuyến đi thất bại");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void btnDeleteMemberOfTravel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int travelId = Int32.Parse(this.cbTravel.SelectedValue.ToString());
            int memberId = Int32.Parse(this.txtMemberOfTravel.Text.ToString());
            
                if (MemberTravelController.Instance().DeleteMemberFromTravel(travelId, memberId))
                {
                    MessageBox.Show("Đã xóa thành viên vào chuyến đi này");
                    this.dtgMemberOfTravel.ItemsSource = MemberTravelController.Instance().GetMembersOfTravel(travelId);
                }
                else
                {
                    MessageBox.Show("Xóa thành viên vào chuyến đi thất bại");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbTravelSpend_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int id = Int32.Parse(this.cbTravelSpend.SelectedValue.ToString());
            this.dtgSpend.ItemsSource = SpendingController.Instance().GetByTravelId(id);
            this.cbMemberTravelSpend.ItemsSource = MemberTravelController.Instance().GetMembersOfTravel(id);
            this.cbMemberTravelSpend.DisplayMemberPath = "name";
            this.cbMemberTravelSpend.SelectedValuePath = "id";
            //DataGridColumn dataGridColumn = dtgSpend.Columns[2];
            //dtgSpend.Columns.Remove(dataGridColumn);
        }

        private void btnAddSpend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int travelId = Int32.Parse(this.cbTravelSpend.SelectedValue.ToString());
            int memberID = Int32.Parse(cbMemberTravelSpend.SelectedValue.ToString());
            float money = float.Parse(this.txtSpendMoney.Text);
            string note = this.txtSpendNote.Text;
            spending spending = new spending();
            spending.member = memberID;
            spending.travel = travelId;
            spending.spendingMoney = money;
            spending.content = note;
            
                if (SpendingController.Instance().Add(spending))
                {
                    MessageBox.Show("Thêm khoản chi thành công");
                    this.dtgSpend.ItemsSource = SpendingController.Instance().GetByTravelId(travelId);
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEditSpend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int id = Int32.Parse(this.txtSpendId.Text);
            int travelId = Int32.Parse(this.cbTravelSpend.SelectedValue.ToString());
            int memberID = Int32.Parse(cbMemberTravelSpend.SelectedValue.ToString());
            float money = float.Parse(this.txtSpendMoney.Text);
            string note = this.txtSpendNote.Text;
            spending spending = new spending();
            spending.member = memberID;
            spending.travel = travelId;
            spending.spendingMoney = money;
            spending.content = note;
            spending.id = id;
            
                if (SpendingController.Instance().Edit(spending))
                {
                    MessageBox.Show("Sửa khoản chi thành công");
                    this.dtgSpend.ItemsSource = SpendingController.Instance().GetByTravelId(travelId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeleteSpend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int id = Int32.Parse(this.txtSpendId.Text);
            int travelId = Int32.Parse(this.cbTravelSpend.SelectedValue.ToString());
            int memberID = Int32.Parse(cbMemberTravelSpend.SelectedValue.ToString());
            float money = float.Parse(this.txtSpendMoney.Text);
            string note = this.txtSpendNote.Text;
            spending spending = new spending();
            spending.member = memberID;
            spending.travel = travelId;
            spending.spendingMoney = money;
            spending.content = note;
            spending.id = id;
           
                if (SpendingController.Instance().Delete(id))
                {
                    MessageBox.Show("Xóa khoản chi thành công");
                    this.dtgSpend.ItemsSource = SpendingController.Instance().GetByTravelId(travelId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStaticTravel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int id = Int32.Parse(this.TxtTravelId.Text);
                new TravelStatic(id).Show();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string keyword = this.txtSearch.Text;
            this.dtgTravel.ItemsSource = TravelController.Instance().Search(keyword);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.dtgTravel.ItemsSource = TravelController.Instance().GetPastTravel();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.dtgTravel.ItemsSource = TravelController.Instance().GetCurrentTravel();
        }

        private void btnDeleteMember_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            string keyword = this.txtMemberOfTravelSearch.Text;
            int travelId = Int32.Parse(this.cbTravel.SelectedValue.ToString());
            this.dtgMemberOfTravel.ItemsSource = MemberTravelController.Instance().SearchMemberOfTravel(travelId, keyword);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                bmImg.Source = new BitmapImage(new Uri(op.FileName));
                this.txtImage.Text = op.FileName;
            }
        }

        private void cbTravel_Copy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int travelId = Int32.Parse(this.cbTravel_Copy.SelectedValue.ToString());
            this.dtgMilestone.ItemsSource = MilestonesController.Instance().GetByTravelId(travelId);

        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
           try
            {
                int travelId = Int32.Parse(this.cbTravel_Copy.SelectedValue.ToString());
                milestone milestone = new milestone();
                milestone.travel = travelId;
                milestone.milestome = txtMileStone.Text;
                milestone.img = txtImage.Text;
                if(MilestonesController.Instance().Add(milestone))
                {
                    MessageBox.Show("Thêm lộ trình thành công");
                    this.dtgMilestone.ItemsSource = MilestonesController.Instance().GetByTravelId(travelId);
                }    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            try
            {
                int travelId = Int32.Parse(this.cbTravel_Copy.SelectedValue.ToString());
                milestone milestone = new milestone();
                milestone.id = Int32.Parse(this.txtMileStoneId.Text);
                milestone.travel = travelId;
                milestone.milestome = txtMileStone.Text;
                milestone.img = txtImage.Text;
                if (MilestonesController.Instance().Edit(milestone))
                {
                    MessageBox.Show("Sửa lộ trình thành công");
                    this.dtgMilestone.ItemsSource = MilestonesController.Instance().GetByTravelId(travelId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            try
            {
                int travelId = Int32.Parse(this.cbTravel_Copy.SelectedValue.ToString());
                milestone milestone = new milestone();
                milestone.id = Int32.Parse(this.txtMileStoneId.Text);
                milestone.travel = travelId;
                milestone.milestome = txtMileStone.Text;
                milestone.img = txtImage.Text;
                if (MilestonesController.Instance().Delete(Int32.Parse(this.txtMileStoneId.Text)))
                {
                    MessageBox.Show("Xóa lộ trình thành công");
                    this.dtgMilestone.ItemsSource = MilestonesController.Instance().GetByTravelId(travelId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
