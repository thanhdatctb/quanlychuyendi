﻿using LiveCharts;
using LiveCharts.Wpf;
using QuanLyChuyenDi.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLyChuyenDi.View
{
    /// <summary>
    /// Interaction logic for TravelStatic.xaml
    /// </summary>
    public partial class TravelStatic : Window
    {
        private Model.travel travel;
        private double averageSpend;
        public TravelStatic(int id)
        {
            InitializeComponent();
            travel = TravelController.Instance().GetById(id);
            averageSpend = SpendingController.Instance().GetAverageByTravel(id);
            //Ưthis.pcSpend = new LiveCharts.Wpf.PieChart();
            LoadData();
           //this.pcSpend.Series;
        }
        public void LoadData()
        {
            this.lbTravelId.Content = travel.id;
            this.lbTravelPlace.Content = travel.place;
            this.lbTravelBegin.Content = travel.startTime;
            this.lbTravelEnd.Content = travel.endTime;
            this.lbTotalSpend.Content = SpendingController.Instance().GetTotalByTravel(travel.id);
            this.lbAverageSpend.Content = averageSpend;
            this.dtgMemberOfTravel.ItemsSource = MemberTravelController.Instance().GetMembersOfTravel(travel.id);
            this.dtgSpend.ItemsSource = SpendingController.Instance().GetByTravelId(travel.id);
            //this.dtgSpend_Copy.ItemsSource = SpendingController.Instance().GetByTravelId(travel.id);
            //setDataForPieChart();
            loadPieChart();
            
        }
        private void loadPieChart()
        {
            Func<ChartPoint, string> labelPoint = chartPoint => string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);

            // Define a collection of items to display in the chart 
            SeriesCollection piechartData = new SeriesCollection();
    //{
    //    new PieSeries
    //    {
    //        Title = "First Item",
    //        Values = new ChartValues<double> {25},
    //        DataLabels = true,
    //        //LabelPoint = labelPoint
    //    },
        
        
    //};

            // You can add a new item dinamically with the add method of the collection
            // Useful when you define the data dinamically and not statically
            var list = SpendingController.Instance().GetByTravelId(travel.id);
            list.ForEach(s =>
            {
                piechartData.Add(new PieSeries
                {
                    Title = s.content,
                    Values = new ChartValues<double> { (double) s.spendingMoney },
                    DataLabels = true,
                    LabelPoint = labelPoint,
                    //Fill = System.Windows.Media.Brushes.Gray
                });
            });
            //piechartData.Add(
            //    new PieSeries
            //    {
            //        Title = "Fourth Item",
            //        Values = new ChartValues<double> { 25 },
            //        DataLabels = true,
            //        LabelPoint = labelPoint,
            //        Fill = System.Windows.Media.Brushes.Gray
            //    }
            //);

            // Define the collection of Values to display in the Pie Chart
            pcSpend.Series = piechartData;

            // Set the legend location to appear in the Right side of the chart
            pcSpend.LegendLocation = LegendLocation.Right;
        }
        private void setDataForPieChart()
        {

            Func<ChartPoint, string> labelPoint = chartPoint =>
                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);
            var spends = SpendingController.Instance().GetByTravelId(travel.id);
            
            var se = new PieSeries
            {
                Title = "Charlessssssssssssssssss",
                Values = new ChartValues<double> { 4 },
                DataLabels = true,
                //LabelPoint = labelPoint
            };
            this.pcSpend.Series.Add(se);
            spends.ForEach(
                e =>
                {
                    this.pcSpend.Series.Add(new PieSeries()
                    {
                        Title = e.content,
                        Values = new ChartValues<double> { (double)e.spendingMoney },
                        PushOut = 15,
                        DataLabels = true,
                        //LabelPoint = labelPoint
                    });
                });
            pcSpend.LegendLocation = LegendLocation.Bottom;
        }
        private void dtgMemberOfTravel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int MemberId = Int32.Parse(this.txtMemberId.Text);
            double spend = SpendingController.Instance().GetTotalByMemberInTravel(MemberId, travel.id);
            this.lbMemberSpend.Content = spend;
            if(this.averageSpend > spend )
            {
                this.lbMemberReceiveLable.Content = "Phải trả cho nhóm trưởng: ";
            }    
            else
            {
                this.lbMemberReceiveLable.Content = "Nhận lại từ nhóm trưởng nhóm trưởng: ";
            }
            this.lbMemberReceive.Content = Math.Abs(averageSpend - spend).ToString();
        }

        private void pcSpend_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void pcSpend_Loaded_1(object sender, RoutedEventArgs e)
        {

        }

        private void pcSpend_Loaded_2(object sender, RoutedEventArgs e)
        {

        }
    }
}
