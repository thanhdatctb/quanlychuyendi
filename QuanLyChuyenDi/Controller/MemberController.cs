﻿using QuanLyChuyenDi.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class MemberController
    {
        private Model1 db = new Model1();
        public static MemberController memberController = null;
        public static MemberController Instance()
        {
            if(memberController == null)
            {
                memberController = new MemberController();
            }
            return memberController;
        }
        public List<member> GetAll()
        {
            return db.members.Where(e=>e.status == true).ToList();
            
        }
        public member GetById(int id)
        {
            return db.members.Where(e => e.id == id).FirstOrDefault();
        }
        public bool Add(member member)
        {
            member.status = true;
            db.members.Add(member);
            db.SaveChanges();
            return true;
        }
        public bool Edit(member member)
        {
            member.status = true;
            db.members.AddOrUpdate(member);
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            member member = this.GetById(id);
            member.status = false;
            db.members.AddOrUpdate(member);
            db.SaveChanges();
            return true;
        }
        
    }
}
