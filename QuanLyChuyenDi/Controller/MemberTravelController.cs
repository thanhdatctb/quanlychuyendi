﻿using QuanLyChuyenDi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class MemberTravelController
    {
        private Model.Model1 db = new Model.Model1();
        private static MemberTravelController memberTravelController = null;
        public static MemberTravelController Instance()
        {
            if (memberTravelController == null)
            {
                memberTravelController = new MemberTravelController();
            }
            return memberTravelController;
        }
        public List<member> GetMembersOfTravel(int travelId)
        {
            List<member> members = new List<member>();
            var memberTravels = db.memberTravels.Where(e => e.travel == travelId)
                .ToList();
            memberTravels.ForEach(x => members.Add(x.member1));
            return members;
                //.GroupBy(e => e.id).Select(gr => gr.First()).ToList();
        }
        public bool AddMemberToTravel(int travelId, int memberId)
        {
            var list = db.memberTravels.ToList();
            //var l = list.Where(e => e.travel == travelId && e.member == travelId).ToList();
            memberTravel memberTravel = db.memberTravels.Where(e => e.travel == travelId && e.member == memberId).ToList().FirstOrDefault();
            if(memberTravel != null)
            {
                return false;
            }
            memberTravel = new memberTravel();

            memberTravel.member = memberId;
            memberTravel.travel = travelId;
            db.memberTravels.Add(memberTravel);
            db.SaveChanges();
            return true;
        }
        public bool DeleteMemberFromTravel(int travelId, int memberId)
        {
            memberTravel memberTravel = db.memberTravels.Where(e => e.travel == travelId && e.member == memberId).FirstOrDefault();
            db.memberTravels.Remove(memberTravel);
            db.SaveChanges();
            return true;
        }

        public List<member> SearchMemberOfTravel(int travelId, string keyword)
        {
            var list = this.GetMembersOfTravel(travelId);
            return list.Where(s => s.name.Contains(keyword)).ToList();
        }
    }
}
