﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class PasswordController
    {
        private static PasswordController passwordController = null;
        public static PasswordController Instance()
        {
            if(passwordController == null)
            {
                passwordController = new PasswordController();
            }
            return passwordController;
        }
        //Hàm mã hóa pasword theo thuật toán SHA-1
        public string Sha1Hash(String password)//Tham số truyền vào là password chưa được mã hóa
        {
            var data = Encoding.ASCII.GetBytes(password);//CHuyển password truyền vào thành mã ASCII
            var hashData = new SHA1Managed().ComputeHash(data);//Băm mã ASCII vừa tạo theo thuật toán SHA-1
            var hash = string.Empty;//Tạo ra 1 chuỗi hash rỗng
            foreach (var b in hashData)
            {
                hash += b.ToString("X2");//Lấy từng kí tự vừa băm chuyển thành mã unicode thường dùng sau đó thêm vào cuối chuỗi hash
            }
            return hash;//Trả về chuỗi hash. Đó chính là password sau khi được mã hóa
            //var md5 = new MD5CryptoServiceProvider();
            //var data = Encoding.ASCII.GetBytes(password);
            //var md5data = md5.ComputeHash(data);
            //var hashedPassword = ASCIIEncoding.GetString(md5data);
            //Console.WriteLine(Encoding.ASCII.GetString(md5data));
            //return Encoding.ASCII.GetString(md5data); 
        }

        //Hàm mã hóa password theo thuật toán md5
        public string MD5Hash(string password) //Tham số truyền vào là password chưa được mã hóa
        {
            var data = Encoding.ASCII.GetBytes(password); //CHuyển password truyền vào thành mã ASCII
            var hashData = new MD5CryptoServiceProvider().ComputeHash(data); //Băm mã ASCII vừa tạo theo thuật toán md5

            //Đoạn này tương tự hàm SHA1Hash ở phía trên
            var hash = string.Empty;
            foreach (var b in hashData)
            {
                hash += b.ToString("X2");
            }
            return hash;
        }

    }
}
