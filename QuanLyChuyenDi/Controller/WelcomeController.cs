﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class WelcomeController
    {
        private Model.Model1 db = new Model.Model1();
        private static WelcomeController adminController = null;
        public static WelcomeController Instance()
        {
            if (adminController == null)
            {
                adminController = new WelcomeController();
            }
            return adminController;
        }
        public bool Check()
        {
            var abc = db.welcomes.Where(s => s.id > -1).FirstOrDefault();
            return (bool)abc.status;

        }
        public void Change()
        {
            var abc = db.welcomes.Where(s => s.id > -1).FirstOrDefault();
            abc.status = false;
            db.welcomes.AddOrUpdate(abc);
            db.SaveChanges();
        }
    }
}
