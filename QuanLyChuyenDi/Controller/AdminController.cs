﻿using QuanLyChuyenDi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class AdminController
    {
        private Model.Model1 db = new Model.Model1();
        private static AdminController adminController = null;
        public static AdminController Instance()
        {
            if (adminController == null)
            {
                adminController = new AdminController();
            }
            return adminController;
        }
        public bool LogIn(string username, string pass)
        {
            string hashpass = PasswordController.Instance().Sha1Hash(pass);
            admin admin = db.admins.Where(s => s.username == username && s.hashpass == hashpass).FirstOrDefault();
            if(admin == null)
            {
                return false;
            }    
            return true;
        }
        public bool Add(admin admin)
        {
            db.admins.Add(admin);
            db.SaveChanges();
            return true;
        }
    }
}
