﻿using QuanLyChuyenDi.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class SpendingController
    {
        private Model.Model1 db = new Model.Model1();
        private static SpendingController spendingController = null;
        public static SpendingController Instance()
        {
            if (spendingController == null)
            {
                spendingController = new SpendingController();
            }
            return spendingController;
        }
        public List<spending> GetAll()
        {
            return db.spendings.Where(s => s.status == true).ToList();
        }
        public spending GetById(int id)
        {
            return db.spendings.Where(s => s.id == id).FirstOrDefault();
        }
        public List<spending> GetByMemberId(int? id)
        {
            return db.spendings.Where(s => s.member == id && s.status == true).ToList();
        }
        public List<spending> GetByTravelId(int id)
        {
            return this.db.spendings.Where(s => s.travel == id && s.status == true).ToList();
        }
        public List<spending> GetByTravelAndMemberId(int memberId, int travelId)
        {
            return this.db.spendings.Where(s => s.travel == travelId && s.member == memberId && s.status == true).ToList();
        }
        public bool Add(spending spending)
        {
            spending.status = true;
            db.spendings.Add(spending);
            db.SaveChanges();
            return true;
        }
        public bool Edit(spending spending)
        {
            spending.status = true;
            db.spendings.AddOrUpdate(spending);
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            spending spending = this.GetById(id);
            spending.status = false;
            db.spendings.AddOrUpdate(spending);
            db.SaveChanges();
            return true;
        }
        public double GetTotalByTravel(int id)
        {
            var list = db.spendings.Where(s => s.travel == id && s.status==true).ToList();
            try
            {
                return (double)list.Sum(e => e.spendingMoney);
            }
            catch(Exception)
            {
                return 0;
            }
            
        }
        public double GetAverageByTravel(int id)
        {
            //var Members = MemberTravelController.Instance().GetMembersOfTravel(id).ToList();
            var travel = TravelController.Instance().GetById(id);
            //var travel = db.travels.Find(id);
            int numberOfMember = travel.memberTravels.Count();
            return this.GetTotalByTravel(id) / numberOfMember;
        }
        public double GetTotalByMemberInTravel(int memberId, int travelId)
        {
            var list = db.spendings.Where(s => s.member == memberId && s.travel == travelId && s.status == true).ToList();
            return (double)list.Sum(s => s.spendingMoney);
            //return 0;
        }
        
    }
}
