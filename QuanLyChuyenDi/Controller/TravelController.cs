﻿using QuanLyChuyenDi.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class TravelController
    {
        private Model.Model1 db = new Model.Model1();
        private static TravelController travelController = null;
        public static TravelController Instance()
        {
            if (travelController == null)
            {
                travelController = new TravelController();
            }
            return travelController;
        }
        public List<Model.travel> GetAll()
        {
            return db.travels.Where(e => e.status == true).ToList();
        }
        public Model.travel GetById(int id)
        {
            return db.travels.Where(e => e.id == id).FirstOrDefault();

        }
        public bool Add(travel travel)
        {
            travel.status = true;
            db.travels.Add(travel);
            db.SaveChanges();
            return true;
        }
        public bool Edit(travel travel)
        {
            travel.status = true;
            db.travels.AddOrUpdate(travel);
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            travel travel = this.GetById(id);
            travel.status = false;
            db.travels.AddOrUpdate(travel);
            db.SaveChanges();
            return true;
        }
        public List<travel> Search(string keyword)
        {
           return db.travels.Where(e => e.status == true && e.place.Contains(keyword)).ToList();
        }
        public List<travel> GetCurrentTravel()
        {
            return db.travels.Where(t => t.endTime > DateTime.Now).ToList();
        }
        public List<travel> GetPastTravel()
        {
            return db.travels.Where(t => t.endTime < DateTime.Now).ToList();
        }
    }
}
