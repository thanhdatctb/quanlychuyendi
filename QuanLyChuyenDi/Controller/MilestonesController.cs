﻿using QuanLyChuyenDi.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyChuyenDi.Controller
{
    class MilestonesController
    {
        private Model1 db = new Model1();
        public static MilestonesController milestonesController = null;
        public static MilestonesController Instance()
        {
            if (milestonesController == null)
            {
                milestonesController = new MilestonesController();
            }
            return milestonesController;
        }
        public List<milestone> GetAll()
        {
            return db.milestones.Where(s => s.status == true).ToList();
        }
        public milestone GetById(int id)
        {
            return db.milestones.Where(s => s.status == true && s.id == id).FirstOrDefault();
        }
        public List<milestone> GetByTravelId(int id)
        {
            return db.milestones.Where(s => s.status == true && s.travel == id).ToList();
        }
        public bool Add(milestone milestone)
        {
            milestone.status = true;
            db.milestones.Add(milestone);
            db.SaveChanges();
            return true;
            
        }
        public bool Edit(milestone milestone)
        {
            milestone.status = true;
            db.milestones.AddOrUpdate(milestone);
            db.SaveChanges();
            return true;

        }
        public bool Delete(int id)
        {
            milestone mi = GetById(id);
            mi.status = false;
            db.milestones.AddOrUpdate(mi);
            db.SaveChanges();
            return false;
        }
    }
}
