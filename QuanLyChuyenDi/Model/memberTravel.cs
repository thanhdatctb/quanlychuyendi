namespace QuanLyChuyenDi.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("memberTravel")]
    public partial class memberTravel
    {
        public int id { get; set; }

        public int? travel { get; set; }

        public int? member { get; set; }

        public virtual member member1 { get; set; }

        public virtual travel travel1 { get; set; }
    }
}
