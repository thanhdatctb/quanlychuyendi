namespace QuanLyChuyenDi.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("spending")]
    public partial class spending
    {
        public int id { get; set; }

        public int? member { get; set; }

        public int? travel { get; set; }

        public double? spendingMoney { get; set; }

        public string content { get; set; }

        public bool? status { get; set; }

        public virtual member member1 { get; set; }

        public virtual travel travel1 { get; set; }
    }
}
