namespace QuanLyChuyenDi.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<admin> admins { get; set; }
        public virtual DbSet<member> members { get; set; }
        public virtual DbSet<memberTravel> memberTravels { get; set; }
        public virtual DbSet<milestone> milestones { get; set; }
        public virtual DbSet<spending> spendings { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<travel> travels { get; set; }
        public virtual DbSet<welcome> welcomes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<admin>()
                .Property(e => e.hashpass)
                .IsUnicode(false);

            modelBuilder.Entity<member>()
                .HasMany(e => e.memberTravels)
                .WithOptional(e => e.member1)
                .HasForeignKey(e => e.member);

            modelBuilder.Entity<member>()
                .HasMany(e => e.spendings)
                .WithOptional(e => e.member1)
                .HasForeignKey(e => e.member);

            modelBuilder.Entity<milestone>()
                .Property(e => e.img)
                .IsUnicode(false);

            modelBuilder.Entity<travel>()
                .HasMany(e => e.memberTravels)
                .WithOptional(e => e.travel1)
                .HasForeignKey(e => e.travel);

            modelBuilder.Entity<travel>()
                .HasMany(e => e.milestones)
                .WithOptional(e => e.travel1)
                .HasForeignKey(e => e.travel);

            modelBuilder.Entity<travel>()
                .HasMany(e => e.spendings)
                .WithOptional(e => e.travel1)
                .HasForeignKey(e => e.travel);
        }
    }
}
