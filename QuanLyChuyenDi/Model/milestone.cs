namespace QuanLyChuyenDi.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class milestone
    {
        public int id { get; set; }

        public int? travel { get; set; }

        [StringLength(100)]
        public string milestome { get; set; }

        [StringLength(20)]
        public string img { get; set; }

        public bool? status { get; set; }

        public virtual travel travel1 { get; set; }
    }
}
