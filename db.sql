use master
go
if (exists (select * from sysdatabases where name = 'QuanLyChuyenDi'))
drop database QuanLyChuyenDi
go
create database QuanLyChuyenDi
go 
use QuanLyChuyenDi
go 
create table admin
(
	id int primary key identity(1,1),
	username nvarchar(100),
	hashpass varchar(100),
	name nvarchar(100)
)
go
create table member
(
	id int primary key identity(1,1),
	name nvarchar(100),
	age int,
	status bit
)
go
create table  travel
(
	id int primary key identity(1,1),
	place nvarchar(100),
	startTime datetime,
	endTime datetime,
	status bit
)
go
create table memberTravel
(
	id int primary key identity(1,1),
	travel int foreign key references travel(id),
	member int foreign key references member(id)
)
go
create table spending
(
	id int primary key identity(1,1),
	member int foreign key references member(id),
	travel int foreign key references travel(id),
	spendingMoney float,
	content nvarchar(max),
	status bit
)
go
create table welcome
(
	id int primary key ,
	status bit
)
go
create table milestones
(
	id int primary key identity(1,1) ,
	travel int foreign key references travel(id),
	milestome nvarchar(100),
	img varchar(20),
	status bit
)
go
insert into welcome values(1,0)
go
--username: admin, pass: admin
insert into admin (username, hashpass,name) values
('admin','d033e22ae348aeb5660fc2140aec35850c4da997', 'admin')